let iterante = true
let userListArray = []
let productListArray = []

class Users {
    constructor(name, lastname, email, phone) {
        this.name = name;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
    }
}

class Products {
    constructor(name, price) {
        this.name = name;
        this.price = price;
    }
}

while (iterante) {
    let opciones = prompt(`
    B I E N V E N I D O !
    Qué desea realizar? Ingrese el número que corresponda

    1: Acceder al módulo de Usuarios;
    2: Acceder al módulo de Productos;    
    3: Realizar un pedido;

    0: Salir.`)
    opciones == 0 ? iterante = false : "";
    let mantenerEnSubMenu = true;
    switch (opciones) {
        case "0":
            break;
        case "1":
            let opcionesUser;
            while (mantenerEnSubMenu) {
                opcionesUser = prompt(`Qué desea realizar? Ingrese el número que corresponda:

    1: Cargar un nuevo Usuario;
    2: Acceder al listado de Usuarios;
    3: Modificar un usuario;
    4: Eliminar un Usuario;

    9: Volver al menú principal.`)
                opcionesUser != "1" && opcionesUser != "2" && opcionesUser != "" && opcionesUser != "3" && opcionesUser != "4" && opcionesUser != "9" ? mantenerEnSubMenu = true : mantenerEnSubMenu = false; //Si opcion elegida es invalida, se mantiene en el mismo menu

            }

            switch (opcionesUser) {
                case "1":
                    agregarUser()
                    break;
                case "2":
                    userListArray.length != "0" ? listarUsers() : alert("No hay usuarios cargados");
                    break;
                case "3":
                    userListArray.length != "0" ? modificarUser() : alert("No hay usuarios cargados");
                    break;
                case "4":
                    userListArray.length != "0" ? eliminarUser() : alert("No hay usuarios cargados");
                    break;
                case "9":
                    break;
                default:
                    alert("entrada invalida")
                    break;
            }
            break;
        case "2":
            let opcionesProducto
            while (mantenerEnSubMenu) {
                opcionesProducto = prompt(`Qué desea realizar? Ingrese el número que corresponda:

    1: Cargar un nuevo Producto;
    2: Acceder al listado de Productos;
    3: Modificar un producto;
    4: Eliminar un Producto;

    9: Volver al menú principal.`)
                opcionesProducto != "1" && opcionesProducto != "2" && opcionesProducto != "" && opcionesProducto != "3" && opcionesProducto != "4" && opcionesProducto != "9" ? mantenerEnSubMenu = true : mantenerEnSubMenu = false; //Si opcion elegida es invalida, se mantiene en el mismo menu
            }
            switch (opcionesProducto) {
                case "1":
                    agregarProducto()
                    break;
                case "2":
                    productListArray.length != "0" ? listarProductos() : alert("No hay productos cargados");
                    break;
                case "3":
                    productListArray.length != "0" ? modificarProducto() : alert("No hay productos cargados");
                    break;
                case "4":
                    productListArray.length != "0" ? eliminarProducto() : alert("No hay productos cargados");
                    break;
                case "9":
                    break;
                default:
                    alert("entrada invalida")
                    break;


            }
            break;
        case "3":
            userListArray.length != "0" && productListArray.length != "0" ? cargarPedido() : alert(`Necesita tener al menos 1 usuario y 1 producto para poder hacer un pedido.
            
Cantidad de usuarios: ${userListArray.length}
Cantidad de productos: ${productListArray.length}`);
            break;
        default:
            iterante = true;
            break;
    }
}



//FUNCIONES PARA LAS POSICIONES DEL MENU

///MENU 1 - 1
function agregarUser() {
    let control = true;
    let errorMessage = "";
    let name;
    while (control) {
        name = prompt("Ingrese Nombre" + errorMessage);
        errorMessage = "";
        !/[^a-z]/i.test(name) && name.length >= 3 ? control = false : (control = true, errorMessage = ". \nPor favor, ingrese sólo letras, minimo de 3 characteres"); //chequea que solo tenga characteres validos y mas de 3 characteres

    }
    let lastname;
    control = true;
    while (control) {
        lastname = prompt("Ingrese Apellido" + errorMessage);
        errorMessage = "";

        !/[^a-z]/i.test(lastname) && lastname.length >= 3 ? control = false : (control = true, errorMessage = ". \nPor favor, ingrese sólo letras, minimo de 3 characteres"); //chequea que solo tenga characteres validos y mas de 3 characteres

    }
    let email;
    control = true;
    while (control) {
        email = prompt("Ingrese Email, ingrese '0' para omitir" + errorMessage);
        errorMessage = "";
        (email.includes('@') && email.includes('.') && email.length > 5) || email == "0" ? control = false : (control = true, errorMessage = ". \nPor favor ingresar un mail valido con este formato, 'usuario@dominio.extension'.") //chequea por punto, @ y length minimo de 5 chars
        email == "0" ? email = "N/A" : null;
    }

    let phone;
    control = true;
    errorMessage = "";
    while (control) {
        phone = prompt("Ingrese Teléfono, ingrese '0' para omitir" + errorMessage);
        errorMessage = "";
        (Number.isInteger(parseInt(phone)) && phone.length >= 6) || phone == "0" ? control = false : (control = true, errorMessage = ". \nPor favor, ingrese sólo números, minimo de 6 characteres. O ingrese un solo '0' para omitir"); // chequea que sea numero, mas de 6 chars, o un cero, sino repite.        
        phone == 0 ? phone = "N/A" : null;
    }


    let nuevoUsuario = new Users(name, lastname, email, phone);
    userListArray.push(nuevoUsuario);
    alert("Usuario '" + nuevoUsuario.name + " " + nuevoUsuario.lastname + "' agendado correctamente.");
    //console.log(userListArray);
}

///MENU 1 - 2
function listarUsers(opc = 0) {
    let listarUsuarios = "Usuarios Cargados: ";
    for (let i = 0; i < userListArray.length; i++) {
        listarUsuarios += "\n" + (i + 1) + ". " + userListArray[i].name + " " + userListArray[i].lastname + " >  @: " + userListArray[i].email + ", Tel: " + userListArray[i].phone + "."
    }
    opc == 0 ? alert(listarUsuarios) : "";
    return listarUsuarios;
}

///MENU 1 - 3
function modificarUser() {
    let listarUsuarios = listarUsers(1);

    let usuarioElegido = parseInt(prompt("Ingrese el número correspondiente al usuario que quiere modificar: \n" + listarUsuarios)); //devuelve el index +1 del usuario
    if (usuarioElegido > userListArray.length || usuarioElegido <= 0 || isNaN(usuarioElegido)) {
        alert("El numero elegido esta fuera de rango, intente nuevamente")
    } else {
        let mantenerEnSubMenu = true;
        while (mantenerEnSubMenu) {
            let propiedadElegida = parseInt(prompt(`Ingrese la propiedad que desea cambiar:

        1. Nombre;
        2. Apellido;
        3. Correo electronico;
        4. Nro de telefono;

        0. Salir sin modificar;`));

            propiedadElegida != 1 && propiedadElegida != 2 && propiedadElegida != "" && propiedadElegida != 3 && propiedadElegida != 4 && propiedadElegida != 0 ? mantenerEnSubMenu = true : mantenerEnSubMenu = false; //Si opcion elegida es invalida, se mantiene en el mismo menu

            let nuevoValor;
            switch (propiedadElegida) {
                case 1: //Nombre
                    nuevoValor = prompt("Ingrese el nuevo Nombre:");
                    userListArray[usuarioElegido - 1].name = nuevoValor;
                    alert("Nombre editado correctamente '" + nuevoValor + "'.");
                    break;
                case 2: //Apellido
                    nuevoValor = prompt("Ingrese el nuevo Apellido:");
                    userListArray[usuarioElegido - 1].lastname = nuevoValor;
                    alert("Apellido editado correctamente '" + nuevoValor + "'.");
                    break;
                case 3: //Email
                    nuevoValor = prompt("Ingrese el nuevo Email:");
                    userListArray[usuarioElegido - 1].email = nuevoValor;
                    alert("Email editado correctamente: '" + nuevoValor + "'.");
                    break;
                case 4: //phone number
                    nuevoValor = prompt("Ingrese el nuevo Telefono:");
                    userListArray[usuarioElegido - 1].phone = nuevoValor;
                    alert("Telefono editado correctamente: '" + nuevoValor + "'.");
                    break;
                default:
                    break;
            }
        }

    }
}

///MENU 1 - 4
function eliminarUser() {
    let listarUsuarios = listarUsers(1);
    let usuarioElegido = parseInt(prompt("Ingrese el número correspondiente al usuario que quiere eliminar: \n" + listarUsuarios)); //devuelve el index +1 del usuario
    if (usuarioElegido > userListArray.length || usuarioElegido <= 0 || isNaN(usuarioElegido)) {
        alert("El numero elegido esta fuera de rango, intente nuevamente")
    } else {
        let usuarioeliminado = userListArray[usuarioElegido - 1].name;
        userListArray.splice((usuarioElegido - 1), 1)
        listarUsuarios = listarUsers(1);
        alert("El usuario '" + usuarioeliminado + "' ha sido eliminado correctamente. \n" + listarUsuarios)
    }
}

///MENU 2 - 1
function agregarProducto() {
    let control = true
    let errorMessage = ""
    let name
    while (control) {
        name = prompt("Ingrese Nombre de Producto" + errorMessage);
        errorMessage = "";
        name.length >= 2 ? control = false : (control = true, errorMessage = ". \nPor favor, ingrese minimo de 2 characteres"); //chequea que tenga mas de 2 characteres

    }

    let price
    control = true
    errorMessage = ""
    while (control) {
        price = prompt("Ingrese Precio de Producto [$]" + errorMessage);
        errorMessage = "";
        (Number.isInteger(parseInt(price)) && price >= 0) ? control = false: (control = true, errorMessage = ". \nPor favor, ingrese sólo números positivos, igual o mayor a '0'"); // chequea que sea numero mayor a cero

    }
    var nuevoProducto = new Products(name, price);
    productListArray.push(nuevoProducto);
    alert("Producto creado correctamente:" + "\n\n" + nuevoProducto.name + " - $" + nuevoProducto.price);
    return productListArray;
}
///MENU 2 - 2
function listarProductos(opc = 0) {
    let listaProducto = "Productos Cargados: " //String para ir mostrando los productos.
    for (let i = 0; i < productListArray.length; i++) {
        listaProducto += "\n" + (i + 1) + ". " + productListArray[i].name + " - $" + productListArray[i].price //Genera una linea de texto con cada producto.
    }
    opc == 0 ? alert(listaProducto) : ""
    return listaProducto;
}

///MENU 2 - 3
function modificarProducto() {
    let listarProductos_s = listarProductos(1);
    let productoElegido = parseInt(prompt("Ingrese el número correspondiente al usuario que quiere modificar: \n" + listarProductos_s)); //devuelve el index +1 del usuario
    if (productoElegido > productListArray.length || productoElegido <= 0 || isNaN(productoElegido)) {
        alert("El numero elegido esta fuera de rango, intente nuevamente")
    } else {
        let mantenerEnSubMenu = true;
        while (mantenerEnSubMenu) {
            let propiedadElegida = parseInt(prompt(`Ingrese la propiedad que desea cambiar:

        1. Nombre del producto;
        2. Precio;

        0. Salir sin modificar;`));
            propiedadElegida != 1 && propiedadElegida != 2 && propiedadElegida != "" && propiedadElegida != 0 ? mantenerEnSubMenu = true : mantenerEnSubMenu = false; //Si opcion elegida es invalida, se mantiene en el mismo menu
            let nuevoValor;
            switch (propiedadElegida) {
                case 1: //Nombre
                    nuevoValor = prompt("Ingrese el nuevo Nombre del producto:");
                    productListArray[productoElegido - 1].name = nuevoValor;
                    alert("Nombre de producto editado correctamente: '" + nuevoValor + "'.");
                    break;
                case 2: //precio
                    nuevoValor = parseInt(prompt("Ingrese el nuevo valor del producto:"));
                    productListArray[productoElegido - 1].price = nuevoValor;
                    alert("Precio editado correctamente: '" + nuevoValor + "'.");
                    break;
                default:
                    break;
            }
        }

    }
}

///MENU 2 - 4
function eliminarProducto() {
    let listarProductos_s = listarProductos(1);
    let productoElegido = parseInt(prompt("Ingrese el número correspondiente al producto que quiere eliminar: \n" + listarProductos_s)); //devuelve el index +1 del producto
    if (productoElegido > productListArray.length || productoElegido <= 0 || isNaN(productoElegido)) {
        alert("El numero elegido esta fuera de rango, intente nuevamente")
    } else {
        let productoeliminado = productListArray[productoElegido - 1].name;
        productListArray.splice((productoElegido - 1), 1)
        listarProductos_s = listarProductos(1);
        alert("El producto '" + productoeliminado + "' ha sido eliminado correctamente. \n" + listarProductos_s)
    }
}


///MENU 3
function cargarPedido() {
    let listarUsuarios = listarUsers(1);
    let mantenerEnSubMenu = true;
    let usuarioElegido;
    while (mantenerEnSubMenu) {
        usuarioElegido = prompt("Ingrese el número correspondiente al Usuario. Para salir, escriba 'salir': \n" + listarUsuarios)
        usuarioElegido <= userListArray.length && usuarioElegido > "0" ? mantenerEnSubMenu = false : "";
    }

    let comprando = true;
    let canasta = []
    let acumulador = 0
    let contador = 0

    while (comprando) {
        let listaProds = listarProductos(1);
        let productoPedido;
        while (mantenerEnSubMenu) {
            productoPedido = prompt("Ingrese el número correspondiente al producto. Para salir, escriba 'Z': \n\n" + "Precio Total: $" + acumulador + "\nCantidad de productps:" + contador + "\n\n" + listaProds) //devuelve el index +1 del producto
            productoPedido > "0" && productoPedido <= productListArray.length ? mantenerEnSubMenu = false : "";

        }

        if (productoPedido.toLocaleLowerCase() != "z") {
            contador++
            canasta.push(productListArray[parseInt(productoPedido) - 1].name)
            acumulador += parseInt((productListArray[parseInt(productoPedido) - 1].price))
            console.log(canasta)
            console.log(acumulador)
        } else {
            comprando = false
        }
    }

    alert(`El usuario ${userListArray[(usuarioElegido - 1)].name} ${userListArray[(usuarioElegido - 1)].lastname}, ha comprado exitosamente ${contador} productos, por un total de $${acumulador}
    Los productos son: ${JSON.stringify(canasta)}.`)
}

alert("Programa Terminado, presione F5 para Recargar. Gracias!");